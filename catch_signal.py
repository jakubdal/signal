#!/usr/bin/env python
import signal, os

def signal_handler(sig, frame):
    os.system("touch signal_called")
    exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.pause()
