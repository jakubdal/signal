package main

import (
	"log"
	"os/exec"
	"syscall"
)

func main() {
	cmd := exec.Command("python3", "catch_signal.py")
	if err := cmd.Start(); err != nil {
		log.Fatalf("cmd.Start: %v", err)
	}

	// if you use CTRL + C instead - the program exits correctly and touches file
	if err := cmd.Process.Signal(syscall.SIGINT); err != nil {
		log.Fatalf("cmd.Process.Signal: %v", err)
	}

	if err := cmd.Wait(); err != nil {
		log.Fatalf("cmd.Process.Wait: %v", err)
	}
}
